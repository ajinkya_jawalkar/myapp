package com.rudysolano.myapp.helper;

import android.app.Activity;

/**
 * Created by zcon on 4/8/16.
 */
public class VersionHelper {
    public static void refreshActionBarMenu(Activity activity)
    {
        activity.invalidateOptionsMenu();
    }
}
