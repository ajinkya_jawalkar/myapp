package com.rudysolano.myapp.helper;

/**
 * Created by zcon on 3/8/16.
 */
public class Habit {
    private String habit, points, streak;

    public Habit() {
    }

    public Habit(String habit,String points,String streak) {
        this.habit = habit;
        this.points = points;
        this.streak = streak;

    }
    public String getHabit() {
        return habit;
    }

    public void setHabit(String habit) {
        this.habit = habit;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getStreak() {
        return streak;
    }

    public void setStreak(String streak) {
        this.streak = streak;
    }

}