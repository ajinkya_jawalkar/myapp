package com.rudysolano.myapp.activity;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;

import com.rudysolano.myapp.R;
import com.rudysolano.myapp.adapter.HabitAdapter;
import com.rudysolano.myapp.helper.DividerItemDecoration;
import com.rudysolano.myapp.helper.Habit;
import com.rudysolano.myapp.helper.TodoListSQLHelper;
import com.rudysolano.myapp.helper.VersionHelper;
import com.skyfishjy.library.RippleBackground;

import java.util.ArrayList;
import java.util.List;

public class HabitActivity extends AppCompatActivity {

    private List<Habit> habitList = new ArrayList<>();
    private RecyclerView recyclerView;
    private HabitAdapter mAdapter;
    private TodoListSQLHelper todoListSQLHelper;
    private Toolbar toolbar;
    private Habit habit;
    private TextInputLayout input_layout_habit;
    private ListAdapter todoListAdapter;
    private Menu menu;
    private boolean selectedFlag = false;
    private int selectedPosition;
    private View view_temp;
    private int position_temp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_habit);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);


        mAdapter = new HabitAdapter(habitList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        // set the adapter
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {
            @Override
            public void onLongClick(View view, int position) {
                selectedFlag = true;
                VersionHelper.refreshActionBarMenu(HabitActivity.this);
                Habit habit = habitList.get(position);
                selectedPosition = position;
                view_temp = view;
                view.setBackgroundDrawable(getResources().getDrawable(R.drawable.card_border));
               // editHabit(position);
            }
        }));
        updateTodoList();
    }

    private void editHabit(final int position) {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);
        View mView = layoutInflaterAndroid.inflate(R.layout.custom_edit_habit, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);
        alertDialogBuilderUserInput.setView(mView);
        input_layout_habit = (TextInputLayout) mView.findViewById(R.id.input_layout_habit);
        final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
        userInputDialogEditText.setText(habitList.get(position).getHabit());
        alertDialogBuilderUserInput
                .setCancelable(true)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        // ToDo get user input here
                        String habitName = userInputDialogEditText.getText().toString().trim();
                        if (habitName.isEmpty() || habitName.trim().toString().equals("") || habitName.trim().toString().equals("")) {
                            Toast.makeText(getApplicationContext(), "Habit name can not be blank", Toast.LENGTH_SHORT).show();
                            return;
                        } else {
                            todoListSQLHelper = new TodoListSQLHelper(HabitActivity.this);
                            SQLiteDatabase sqLiteDatabase = todoListSQLHelper.getWritableDatabase();
                            //write the Todo task input into database table
                            sqLiteDatabase.execSQL("UPDATE " + TodoListSQLHelper.TABLE_NAME + " SET " + TodoListSQLHelper.COL1_HABIT + "= '" + habitName + "' WHERE " + TodoListSQLHelper.COL1_HABIT + " ='" + habitList.get(position).getHabit() + "'");
                            updateTodoList(position, habitName, "0", "0");
                        }

                    }
                })

                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                dialogBox.cancel();
                            }
                        });

        final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#2667c8"));
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#2667c8"));
                alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(16f);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(16f);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).getBackground().setAlpha(255);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).getBackground().setAlpha(255);

            }
        });
        alertDialogAndroid.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.card_border));
        alertDialogAndroid.show();

    }

    public interface ClickListener {
        void onLongClick(View view, int position);
    }

    private void prepareHabitData(String habitName,String points, String streak) {
        habit = new Habit(habitName, "0", "0");
        habitList.add(habit);

    }
    private void prepareHabitData(int position, String habitName,String points, String streak) {
        habitList.remove(position);
        habit = new Habit(habitName, "0", "0");
        habitList.add(position, habit);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(!selectedFlag){
            VersionHelper.refreshActionBarMenu(HabitActivity.this);
            getMenuInflater().inflate(R.menu.menu_habit, menu);
        }

        else
        {
            VersionHelper.refreshActionBarMenu(HabitActivity.this);
            getMenuInflater().inflate(R.menu.menu_habit_edit, menu);
        }

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_task:
                LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);
                View mView = layoutInflaterAndroid.inflate(R.layout.custom_add_habit, null);
                AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);
                alertDialogBuilderUserInput.setView(mView);
                input_layout_habit = (TextInputLayout) mView.findViewById(R.id.input_layout_habit);
                final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
                alertDialogBuilderUserInput
                        .setCancelable(true)
                        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogBox, int id) {
                                // ToDo get user input here
                                String habitName = userInputDialogEditText.getText().toString().trim();
                                if (habitName.isEmpty() || habitName.trim().toString().equals("") || habitName.trim().toString().equals("")) {
                                    Toast.makeText(getApplicationContext(), "Habit name can not be blank", Toast.LENGTH_SHORT).show();
                                    return;
                                } else {
                                    todoListSQLHelper = new TodoListSQLHelper(HabitActivity.this);
                                    SQLiteDatabase sqLiteDatabase = todoListSQLHelper.getWritableDatabase();
                                    if (habitList.size() < 10) {
                                        ContentValues values = new ContentValues();
                                        values.clear();

                                        //write the Todo task input into database table
                                        values.put(TodoListSQLHelper.COL1_HABIT, habitName);
                                        sqLiteDatabase.insertWithOnConflict(TodoListSQLHelper.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                                        updateTodoList(habitName, "0", "0");
                                        mAdapter.notifyDataSetChanged();
                                    } else {
                                        dialogBox.cancel();
                                        exceedLimitMsg();
                                    }
                                }
                            }
                        })

                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialogBox, int id) {
                                        dialogBox.cancel();
                                    }
                                });

                final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
                alertDialogAndroid.setOnShowListener(new DialogInterface.OnShowListener() {
                                                         @Override
                                                         public void onShow(DialogInterface arg0) {
                                                             alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#2667c8"));
                                                             alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#2667c8"));
                                                             alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(16f);
                                                             alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(16f);
                                                             alertDialogAndroid.getButton(AlertDialog.BUTTON_NEGATIVE).getBackground().setAlpha(255);
                                                             alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).getBackground().setAlpha(255);
                                                         }
                                                     });
                alertDialogAndroid.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.card_border));
                alertDialogAndroid.show();
                return true;

            case R.id.action_edit_task:
                    editHabit(selectedPosition);
                    selectedFlag = false;
                    view_temp.setBackgroundDrawable(getResources().getDrawable(R.drawable.list_border));
                    VersionHelper.refreshActionBarMenu(HabitActivity.this);
                    //getMenuInflater().inflate(R.menu.menu_habit, menu);
                return  true;

            case R.id.action_delete_task:
                Toast.makeText(getApplicationContext(),"Delete",Toast.LENGTH_SHORT).show();
                return  true;

            case R.id.action_back_task:
                selectedFlag = false;
                view_temp.setBackgroundDrawable(getResources().getDrawable(R.drawable.list_border));
                VersionHelper.refreshActionBarMenu(HabitActivity.this);
                return  true;

            default:
                return false;
        }
    }

    private void exceedLimitMsg() {
        LayoutInflater layoutInflaterAndroid = LayoutInflater.from(this);
        View mView = layoutInflaterAndroid.inflate(R.layout.custom_exceed_habit, null);
        AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(this);
        alertDialogBuilderUserInput.setView(mView);
        input_layout_habit = (TextInputLayout) mView.findViewById(R.id.input_layout_habit);
        final EditText userInputDialogEditText = (EditText) mView.findViewById(R.id.userInputDialog);
        alertDialogBuilderUserInput
                .setCancelable(true)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogBox, int id) {
                        // ToDo get user input here
                        dialogBox.cancel();
                    }
                });
        final AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
        alertDialogAndroid.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#2667c8"));
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(16f);
                alertDialogAndroid.getButton(AlertDialog.BUTTON_POSITIVE).getBackground().setAlpha(255);
            }
        });
        //mView.setBackgroundResource(R.drawable.card_border);
        alertDialogAndroid.getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.card_border));
        alertDialogAndroid.show();

    }

    //update the todo task list UI
    private void updateTodoList(int position, String habit,String points,String streak) {
        prepareHabitData(position, habit, points, streak);
        mAdapter.notifyDataSetChanged();
    }
    //update the todo task list UI
    private void updateTodoList(String habit,String points,String streak) {
        prepareHabitData(habit,points,streak);
        mAdapter.notifyDataSetChanged();
    }


    //update the todo task list UI
    private void updateTodoList() {
        todoListSQLHelper = new TodoListSQLHelper(HabitActivity.this);
        SQLiteDatabase sqLiteDatabase = todoListSQLHelper.getReadableDatabase();
//          Delete all  the rows from table
//        sqLiteDatabase.delete(TodoListSQLHelper.TABLE_NAME, null,null);

        List<Habit> habittList = new ArrayList<Habit>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TodoListSQLHelper.TABLE_NAME;
         Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Habit habit = new Habit();
                habit.setHabit(cursor.getString(1));
                habit.setPoints("0");
                habit.setStreak("0");
                // Adding Habits to list
                habittList.add(habit);
            } while (cursor.moveToNext());
        }
        if(habittList!=null && habitList.size()<=10) {
            for (int i = 0; i < habittList.size(); i++) {
                prepareHabitData(habittList.get(i).getHabit(),
                        habittList.get(i).getPoints(),
                        habittList.get(i).getStreak());
            }
        }
        else if(habittList.size()>=11)
        {
            exceedLimitMsg();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Empty Limit",Toast.LENGTH_SHORT).show();
        }
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private HabitActivity.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final HabitActivity.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onLongClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }

    }

}