package com.rudysolano.myapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.rudysolano.myapp.R;
import com.rudysolano.myapp.helper.Habit;

import java.util.List;

/**
 * Created by zcon on 3/8/16.
 */
public class HabitAdapter extends RecyclerView.Adapter<HabitAdapter.MyViewHolder> {

    private List<Habit> habitList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView habit, points, streak;
        private CheckBox today;

        public MyViewHolder(View view) {
            super(view);
            habit = (TextView) view.findViewById(R.id.TextView01);
            points = (TextView) view.findViewById(R.id.TextView02);
            streak = (TextView) view.findViewById(R.id.TextView03);
            today = (CheckBox) view.findViewById(R.id.completeBtn);
        }
    }


    public HabitAdapter(List<Habit> habitList) {
        this.habitList = habitList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.todotask, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Habit habit = habitList.get(position);
        holder.habit.setText(habit.getHabit());
        holder.points.setText(habit.getPoints());
        holder.streak.setText(habit.getStreak());

    }


    @Override
    public int getItemCount() {
        return habitList.size();
    }
}